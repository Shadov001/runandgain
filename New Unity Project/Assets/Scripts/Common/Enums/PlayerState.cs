﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Common
{
    //State Machine (FSM)
    public enum PlayerState
    {
        Idle = 0,
        Running = 1,
        Jumping = 2,
        Falling = 3,
        Hurt = 4,
        Dead = 5,
        Climbing =6
    }
}
