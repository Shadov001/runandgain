﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public static class Consts
    {
        public static Color STRENGTH_POWERUP_PLAYER_COLOR = Color.red;
        public static Color JUMP_POWERUP_PLAYER_COLOR = Color.cyan;
        public static Color PLAYER_DEFAULT_COLOR = Color.white;

        public const float JUMP_FORCE_VALUE = 25f;
        public const float JUMP_FORCE_POWERUP_VALUE = 43f;

        public const float HURT_FORCE_VALUE = 20f;

        public const int INITIAL_HEALTH = 100;

        public const float CLIMBING_SPEED = 3f;
        public const float RUNNING_SPEED = 8f;

        public const float JUMP_POWERUP_DURATION = 7;
        public const float STRENGTH_POWERUP_DURATION = 7;

        public const int FROG_DAMAGE = 10;
        public const int EAGLE_DAMAGE = 20;
        public const int OTHER_ENEMY_DAMAGE = 15;

        public const float FROG_JUMP_LENGTH = 3f;
        public const float FROG_JUMP_HEIGHT = 5f;
    }
}
