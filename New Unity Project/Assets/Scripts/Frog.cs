﻿using Assets.Scripts.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frog : Enemy
{
    private float leftCap;
    private float rightCap;

    private float jumpLength = Consts.FROG_JUMP_LENGTH;
    private float jumpHeight = Consts.FROG_JUMP_HEIGHT;

    [SerializeField] private LayerMask ground;

    private new Collider2D collider;
    private bool facingLeft = true;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        collider = GetComponent<Collider2D>();
        leftCap = transform.position.x - 3;
        rightCap = transform.position.x + 3;
    }

    // Update is called once per frame
    void Update()
    {
        //transition from jumping to falling
        if (animator.GetBool("Jumping"))
        {
            if(rb.velocity.y < .1)
            {
                animator.SetBool("Falling", true);
                animator.SetBool("Jumping", false);
            }
        }
        //transition from fall to idle
        if (collider.IsTouchingLayers(ground) && animator.GetBool("Falling"))
        {
            animator.SetBool("Falling", false);

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Chest")
        {
            ChestController chest = collision.gameObject.GetComponent<ChestController>();
            chest.JumpedOn(collider.tag);
        }
    }

    private void Move()
    {
        if (facingLeft)
        {
            //test to see if we are beyond the leftCap
            if (transform.position.x > leftCap)
            {
                //make sure sprite is facing right location and if it is not, then face the right direction
                if (transform.localScale.x != 1)
                {
                    transform.localScale = new Vector3(1, 1);
                }
                //test to see if I am on the ground, then jump
                if (collider.IsTouchingLayers(ground))
                {
                    //jump
                    rb.velocity = new Vector2(-jumpLength, jumpHeight);
                    animator.SetBool("Jumping", true); 
                }
            }
            else
            {
                facingLeft = false;
            }
        }
        else
        {
            //test to see if we are beyond the leftCap
            if (transform.position.x < rightCap)
            {
                //make sure sprite is facing right location and if it is not, then face the right direction
                if (transform.localScale.x != -1)
                {
                    transform.localScale = new Vector3(-1, 1);
                }
                //test to see if I am on the ground, then jump
                if (collider.IsTouchingLayers(ground))
                {
                    //jump
                    rb.velocity = new Vector2(jumpLength, jumpHeight);
                    animator.SetBool("Jumping", true);
                }
            }
            else
            {
                facingLeft = true;
            }
        }
    }

}
