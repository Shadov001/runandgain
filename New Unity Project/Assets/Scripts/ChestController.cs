﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestController : MonoBehaviour
{
    private Animator animator;

    [SerializeField] private AudioSource destroySound;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void JumpedOn(string colliderTag)
    {
        animator.SetTrigger("ChestDestroyed");
        if(colliderTag == "Player") //play sound only if PLAYER jump on the chest
        {
            destroySound.Play();
        }
        GetComponent<Collider2D>().enabled = false;
    }

    //Animation event triggered after 'DestroyChest' animation
    private void ChestDestroyed()
    {
        Destroy(this.gameObject);
    }

}
