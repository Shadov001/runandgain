﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverController : MonoBehaviour
{

    [SerializeField] private Sprite leverDown;
    [SerializeField] private AudioSource leverDownSound;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            PlayerController player = collision.gameObject.GetComponent<PlayerController>();
            if (GetComponent<SpriteRenderer>().sprite != leverDown)  //if not pulled yet - pull the lever and increment counter
            {
                leverDownSound.Play();
                GetComponent<SpriteRenderer>().sprite = leverDown;
                player.leversCounter++;
            }
        }
    }
}
