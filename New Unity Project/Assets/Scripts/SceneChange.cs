﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneChange : MonoBehaviour
{
    [SerializeField] private string sceneName;
    [SerializeField] private AudioSource finishSound;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //finishSound.Play();
            //StartCoroutine(WaitForFinish());
        }
    }

    private IEnumerator WaitForFinish()
    {
        yield return new WaitForSeconds(1);
        SceneHandler.LoadScene(sceneName);
    }
}
