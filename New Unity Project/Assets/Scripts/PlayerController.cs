﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using Assets.Scripts.Common;
using Assets.Scripts;

public class PlayerController : MonoBehaviour
{
    //Start() variables
    private Coroutine strengthPowerUpRoutine;
    private Coroutine powerUpRoutine;
    private Rigidbody2D rb;
    private Animator animator;
    private new Collider2D collider;
    private PolygonCollider2D polygonCollider;
    private bool isExploding = false;  //if player's death animation is running?
    private bool isFinishing = false;  //if player's finishing animation is running?
    private bool strengthPowerActive = false; //if strength powerup is active?
    public bool canFinishLevel = false;  //if levers are pulled
    public int leversCounter = 0;
    private int leversToPull;

    private PlayerState state = PlayerState.Idle;

    private float jumpForce = Consts.JUMP_FORCE_VALUE;
    private int health = Consts.INITIAL_HEALTH;
    private readonly float hurtForce = Consts.HURT_FORCE_VALUE;
    public float climbSpeed = Consts.CLIMBING_SPEED;
    private readonly float speed = Consts.RUNNING_SPEED;

    //Climbing the ladder variables
    public bool canClimb = false;
    public bool bottomLadder = false;
    public bool topLadder = false;
    public LadderController ladder;
    private float naturalGravity;

    #region Inspector variables

    [SerializeField] private LayerMask ground;
    [SerializeField] private LayerMask deathAreas;
    [SerializeField] private LayerMask collectible;
    [SerializeField] private LayerMask chests;

    [SerializeField] private AudioSource cherry;
    [SerializeField] private AudioSource footstep;
    [SerializeField] private AudioSource hurt;
    [SerializeField] private AudioSource deathSound;

    [SerializeField] private AudioSource finishSound;
    [SerializeField] private AudioSource fallingSound;
    [SerializeField] private AudioSource jumpingSound;

    [SerializeField] private AudioSource powerup;
    [SerializeField] private AudioSource powerupTime;
    [SerializeField] private AudioSource strengthPowerup;
    [SerializeField] private AudioSource strengthPowerupTime;
    [SerializeField] private AudioSource powerUpEnd;
   
    [SerializeField] private int cherries = 0;
    [SerializeField] private TextMeshProUGUI cherryText;

    [SerializeField] private int destroyedChests = 0;
    [SerializeField] private TextMeshProUGUI destroyedChestsText;

    [SerializeField] private TextMeshProUGUI leverCounterText;
    
    [SerializeField] private TextMeshProUGUI healthAmount;

    #endregion

    private void Start()
    {
        GameObject.FindGameObjectWithTag("Music").GetComponent<MusicController>().PlayMusic();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        collider = GetComponent<Collider2D>();
        polygonCollider = GetComponent<PolygonCollider2D>();
        healthAmount.text = health.ToString();

        naturalGravity = rb.gravityScale;

        leversToPull = GameObject.FindGameObjectsWithTag("Lever").Length; //search for all levers to pull
    }

    private void Update()
    {
        if(state == PlayerState.Climbing)
        {
            Climb();
        }
        else if (state != PlayerState.Hurt)
        {
            Movement();
        }

        //Updating actual player state
        AnimationState();
        animator.SetInteger("state", (int)state); //sets animation based on Enumerator state

        //Checking needed ammount of pulled levers to open the final door
        CheckPulledLeversCounter();

        //Updating label of pulled levers status
        UpdateLeverCounterText();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Chest":
                RaycastHit2D jumpRaycast = Physics2D.Raycast(rb.position, Vector2.down, 1.3f, chests);
                ChestController chest = collision.gameObject.GetComponent<ChestController>();
                if (jumpRaycast.collider != null || (jumpRaycast.collider == null && polygonCollider.IsTouchingLayers(chests)))
                {
                    chest.JumpedOn(collider.tag);
                    destroyedChests++;
                    destroyedChestsText.text = destroyedChests.ToString();
                    Jump();
                }
                break;
            //Player entering the DEADZONE
            case "DeathArea":
                hurt.Play();
                animator.SetTrigger("PlayerDead");
                isExploding = true;
                break;
            //Player colliding with the enemy
            case "Enemy":
                Enemy enemy = collision.gameObject.GetComponent<Enemy>();
                if (state == PlayerState.Falling)
                {
                    enemy.EnemyKilled(); //jumped on the enemy
                    Jump();
                }
                else
                {
                    if (strengthPowerActive) //If Strength Power is active -> you will kill the enemy even if run into them
                    {
                        enemy.EnemyKilled();
                    }
                    else
                    {
                        var generatedDamage = GenerateDamage(enemy.name);
                        hurt.Play();
                        state = PlayerState.Hurt;
                        LooseHealth(generatedDamage); //update health and update UI - reset scene when out of health
                        if (collision.gameObject.transform.position.x > transform.position.x)
                        {
                            //Enemy is to my right therefore I should be damaged and pushed to the left
                            rb.velocity = new Vector2(-hurtForce, rb.velocity.y);
                        }
                        else
                        {
                            //Enemy is to my left therefore I should be damaged and pushed to the right
                            rb.velocity = new Vector2(hurtForce, rb.velocity.y);
                        }
                    }
                }
                break;
            default:
                return;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Collecting a cherry
        if (collision.CompareTag("Collectable") && collider.IsTouchingLayers(collectible))
        {
            Destroy(collision.gameObject);
            CollectCherry();
        }

        //Collecting a star powerup (become an enemy slayer)
        if (collision.CompareTag("StrengthPowerup") && collider.IsTouchingLayers(collectible))
        {
            //If any other powerup routine is active - disable them, reset music and gain current powerup
            if (powerUpRoutine != null)
            {
                StopCoroutine(powerUpRoutine);
                powerupTime.Pause();
                jumpForce = 25;
                GetComponent<SpriteRenderer>().color = Color.white;
            }

            Destroy(collision.gameObject);
            if (strengthPowerUpRoutine != null)
            {
                StopCoroutine(strengthPowerUpRoutine);
            }
            //Start routine 
            this.strengthPowerUpRoutine = StartCoroutine(ProvideStrengthPowerup());
        }

        //Collecting a diamond powerup (higher jump, change color of the player)
        if (collision.CompareTag("Powerup"))
        {
            //If any other powerup routine is active - disable them and gain current powerup
            if (strengthPowerUpRoutine != null)
            {
                StopCoroutine(strengthPowerUpRoutine);
                strengthPowerupTime.Pause();
                strengthPowerActive = false;
                GetComponent<SpriteRenderer>().color = Color.white;
            }
            //Destroy the powerup object and check if powerup is already active - if active, then restart it
            Destroy(collision.gameObject);
            if (powerUpRoutine != null)
            {
                StopCoroutine(powerUpRoutine);
            }
            //Start routine 
            this.powerUpRoutine = StartCoroutine(ProvideHigherJumpPowerup());
        }

        //Finishing a level
        if (collision.CompareTag("Finish"))
        {
            if (canFinishLevel)
            {
                FinishLevel();
            }
        }
    }

    private void FinishLevel()
    {
        //TODO: Get rid of Warning on falling into the finish area
        //Stop the hero
        rb.velocity = Vector2.zero;
        rb.bodyType = RigidbodyType2D.Static;
        //Set isFinishing to 'true' so I can deactivate Movement 
        isFinishing = true;
        //Finish animation and sound
        animator.SetTrigger("LevelFinished");
        finishSound.Play();
    }

    private void CollectCherry()
    {
        cherry.Play();
        cherries++;
        cherryText.text = cherries.ToString();
    }

    private void Movement()
    {
        //get predefined Input settings based on axes
        float hDirection = Input.GetAxis("Horizontal");
        float vDirection = Input.GetAxis("Vertical");

        if(canClimb && (Mathf.Abs(Input.GetAxis("Vertical")) > .1f || !collider.IsTouchingLayers(ground)))
        {
            if (!collider.IsTouchingLayers(ground))
            {
                rb.velocity = new Vector2(rb.velocity.x, 0f);
            }
            state = PlayerState.Climbing;
            rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
            transform.position = new Vector3(ladder.transform.position.x, rb.position.y);
            rb.gravityScale = 0f;
        }

        //if player is not exploding, then move
        if (!isExploding && !isFinishing)
        {
            //Moving left
            if (hDirection < 0 && (!collider.IsTouchingLayers(chests) || (collider.IsTouchingLayers(chests) && rb.velocity == Vector2.zero)))
            {
                rb.velocity = new Vector2(-speed, rb.velocity.y);
                transform.localScale = new Vector2(-1, 1);
            }

            //Moving right
            else if (hDirection > 0 && (!collider.IsTouchingLayers(chests) || (collider.IsTouchingLayers(chests) && rb.velocity == Vector2.zero)))
            {
                rb.velocity = new Vector2(speed, rb.velocity.y);
                transform.localScale = new Vector2(1, 1);
            }

            if (Input.GetButtonDown("Jump"))
            {

                RaycastHit2D hit = Physics2D.Raycast(rb.position, Vector2.down, 1.3f, ground);
                if (hit.collider != null || (hit.collider == null && (polygonCollider.IsTouchingLayers(ground) || polygonCollider.IsTouchingLayers(chests))))
                {
                    Jump();
                }
            }
        }
    }

    /// <summary>
    /// Manage player climbing state 
    /// </summary>
    private void Climb()
    {
        if (Input.GetButtonDown("Jump")) //Jump from the ladder
        {
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            canClimb = false;
            rb.gravityScale = naturalGravity;
            animator.speed = 1f;
            Jump();
            return;
        }

        float vDirection = Input.GetAxis("Vertical");

        //Climbing up
        if(vDirection > .1f && !topLadder)
        {
            rb.velocity = new Vector2(0f, vDirection * climbSpeed);
            animator.speed = 1f;
        }
        //Climbing down
        else if (vDirection < -.1f && !bottomLadder)
        {
            rb.velocity = new Vector2(0f, vDirection * climbSpeed);
            animator.speed = 1f;
        }
        //Still
        else
        {
            animator.speed = 0f;
            rb.velocity = Vector2.zero;
        }
    }

    /// <summary>
    /// Return generated damage by enemy by its type
    /// </summary>
    /// <param name="enemyName"></param>
    /// <returns></returns>
    private int GenerateDamage(string enemyName)
    {
        return enemyName.StartsWith("Frog") ? Consts.FROG_DAMAGE
                : enemyName.StartsWith("Eagle") ? Consts.EAGLE_DAMAGE
                : Consts.OTHER_ENEMY_DAMAGE;
    }

    /// <summary>
    /// When player jumps, play specific sound and push him into the air
    /// </summary>
    private void Jump()
    {
        jumpingSound.Play();
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        state = PlayerState.Jumping;
    }

    /// <summary>
    /// Manage player state
    /// </summary>
    private void AnimationState()
    {
        if(state == PlayerState.Climbing)
        {

        }

        else if (state == PlayerState.Jumping)
        {
            if (rb.velocity.y < 0.1f)
            {
                state = PlayerState.Falling;
            }
        }

        else if (state == PlayerState.Falling)
        {
            if (collider.IsTouchingLayers(ground))
            {
                fallingSound.Play();
                state = PlayerState.Idle;
            }
        }

        else if (state == PlayerState.Hurt)
        {
            if (Mathf.Abs(rb.velocity.x) < .1f)
            {
                state = PlayerState.Idle;
            }
        }

        else if (Mathf.Abs(rb.velocity.x) > 1f)
        {
            //Moving
            state = PlayerState.Running;
        }

        else
        {
            //Idle
            state = PlayerState.Idle;
        }
    }

    /// <summary>
    /// Play footstep sound
    /// </summary>
    private void Footstep()
    {
        footstep.Play();
    }

    /// <summary>
    /// Loose ammount of health and if it is too low, start dying process
    /// </summary>
    /// <param name="damage"></param>
    private void LooseHealth(int damage)
    {
        health -= damage;
        healthAmount.text = health.ToString();
        if (health <= 0)
        {
            PlayerIsDying();
        }
    }

    /// <summary>
    /// Manage dying state for the player, including setting animation of exploding, playing death sound and setting trigger for loading current scene again
    /// </summary>
    private void PlayerIsDying()
    {
        GetComponent<SpriteRenderer>().color = Color.white; //explode animation must be white (in case if powerup is active and player color is changed)
        deathSound.Play(); //play deathSound audio
        GetComponent<PlayerController>().enabled = false; //disable player from moving
        animator.SetTrigger("PlayerDead"); //initialize death animation
        isExploding = true; //send information that player is actually exploding
    }

    /// <summary>
    /// Load again current scene when player is dead
    /// </summary>
    private void PlayerDead()
    {
        //Animation event after PlayerDeath animation
        isExploding = false;
        SceneHandler.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Function sets up and manage higher jump powerup for the player when he collect one of them
    /// </summary>
    /// <returns></returns>
    private IEnumerator ProvideHigherJumpPowerup()
    {
        //Play the sound of picking up the powerup and destroy poweorup object
        powerup.Play();

        //Set powerup effects
        jumpForce = Consts.JUMP_FORCE_POWERUP_VALUE;
        GetComponent<SpriteRenderer>().color = Consts.JUMP_POWERUP_PLAYER_COLOR;

        //Play the sound of powerup time
        powerupTime.Play();
        //Wait few seconds and then allow me to do something
        yield return new WaitForSeconds(Consts.JUMP_POWERUP_DURATION);

        //Back to normal state
        powerupTime.Pause();
        powerUpEnd.Play();
        jumpForce = Consts.JUMP_FORCE_VALUE;
        GetComponent<SpriteRenderer>().color = Consts.PLAYER_DEFAULT_COLOR;

    }

    /// <summary>
    /// Function sets up and manage strength powerup for the player when he collect one of them
    /// </summary>
    /// <returns></returns>s
    private IEnumerator ProvideStrengthPowerup()
    {
        //Play the sound of picking up the powerup and destroy poweorup object
        strengthPowerup.Play();

        //Set powerup effects
        strengthPowerActive = true;
        GetComponent<SpriteRenderer>().color = Consts.STRENGTH_POWERUP_PLAYER_COLOR;

        //Play the sound of powerup time
        strengthPowerupTime.Play();

        //Wait few seconds and then allow me to do something
        yield return new WaitForSeconds(Consts.STRENGTH_POWERUP_DURATION);

        //Back to normal state
        strengthPowerupTime.Pause();
        powerUpEnd.Play();
        strengthPowerActive = false;
        GetComponent<SpriteRenderer>().color = Consts.PLAYER_DEFAULT_COLOR;

    }

    /// <summary>
    /// Wait some time after reaching the final door and load ending scene
    /// </summary>
    /// <returns></returns>
    private IEnumerator WaitForLevelFinish()
    {
        yield return new WaitForSeconds((float)0.01);
        SceneHandler.LoadScene("TheEnd");
    }

    /// <summary>
    /// Wait for a specific ammount of time
    /// </summary>
    /// <param name="seconds"></param>
    /// <returns></returns>
    private IEnumerator WaitCustomTime(float seconds)
    {
        yield return new WaitForSeconds((float)seconds);
    }

    /// <summary>
    /// Funcion allows player to finish the level if he pulled all the levers down
    /// </summary>
    private void CheckPulledLeversCounter()
    {
        if (leversCounter == leversToPull)
        {
            canFinishLevel = true;
        }
    }

    /// <summary>
    /// Updates text of leverCounterText
    /// </summary>
    private void UpdateLeverCounterText()
    {
        leverCounterText.text = leversCounter + "/" + leversToPull;
    }
}
