using Assets.Scripts.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderController : MonoBehaviour
{
    [SerializeField] LadderPart part = LadderPart.Complete;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerController>())
        {
            PlayerController player = collision.GetComponent<PlayerController>();
            switch (part)
            {
                case LadderPart.Complete:
                    player.canClimb = true;
                    player.ladder = this;
                    break;
                case LadderPart.Bottom:
                    player.bottomLadder = true;
                    break;
                case LadderPart.Top:
                    player.topLadder = true;
                    break;
                default:
                    break;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerController>())
        {
            PlayerController player = collision.GetComponent<PlayerController>();
            switch (part)
            {
                case LadderPart.Complete:
                    player.canClimb = false;
                    break;
                case LadderPart.Bottom:
                    player.bottomLadder = false;
                    break;
                case LadderPart.Top:
                    player.topLadder = false;
                    break;
                default:
                    break;
            }
        }
    }
}
