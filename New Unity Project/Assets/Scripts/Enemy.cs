﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    protected Animator animator;
    protected Rigidbody2D rb;
    private AudioSource death;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        death = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    private void Update()
    {
        
    }

    public void EnemyKilled()
    {
        animator.SetTrigger("Death");
        death.Play();
        //Set velocity to 0 to prevent Death animation from moving
        rb.velocity = Vector2.zero;
        rb.bodyType = RigidbodyType2D.Static; //to prevent enemy from moving
        GetComponent<Collider2D>().enabled = false; //to prevent player from colliding with frog when its dying
    }

    //Animation event triggered after 'EnemyDeath' animation
    private void Death()
    {
        Destroy(this.gameObject);
    }

}
